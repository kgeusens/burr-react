import React, { useContext, useEffect } from "react";
import { SceneContext } from "./SceneComponent.js";
import { CSG, MeshBuilder, Matrix, Vector3, VertexData } from "@babylonjs/core";

function Shape({ position, shape }) {
  //  const [scene, setScene] = useState(null);
  const { scene } = useContext(SceneContext);
  var shapeMesh = null;
  //  console.log("Shape function", shape);

  useEffect(() => {
    buildShapeMesh();
    return () => {
      if (shapeMesh) shapeMesh.dispose();
    };
  }, [scene]);

  function buildShapeMesh() {
    if (scene == null) return;
    if (shapeMesh) {
      shapeMesh.dispose();
    }
    let delta = 0.1;

    let x = Math.abs(shape.dimensions[0]);
    let sx = Math.sign(shape.dimensions[0]);
    let y = Math.abs(shape.dimensions[1]);
    let sy = Math.sign(shape.dimensions[1]);
    let z = Math.abs(shape.dimensions[2]);
    let sz = Math.sign(shape.dimensions[2]);

    // Create the base volume
    let theShapeMesh = MeshBuilder.CreateBox(
      "outline",
      { width: x - 2 * delta, height: y - 2 * delta, depth: z - 2 * delta },
      scene
    );
    // move the origin to the voxel at [0,0,0]
    theShapeMesh.setPivotMatrix(
      Matrix.Translation(
        (sx * (x - 1)) / 2,
        (sy * (y - 1)) / 2,
        (sz * (z - 1)) / 2
      ),
      false
    );
    // punch the holes (requires CSG)
    var theCSG = CSG.FromMesh(theShapeMesh);
    var hole = MeshBuilder.CreateBox("hole", { size: 1 + 2 * delta }, scene);
    shape.holePositions.forEach((h) => {
      hole.position = new Vector3(h[0], h[1], h[2]);
      theCSG.subtractInPlace(CSG.FromMesh(hole));
    });
    hole.dispose();
    // update the vertices in theShapeMesh based on the final CSG
    var tempMesh = theCSG.toMesh("csg1", null, scene);
    var vertexData = VertexData.ExtractFromMesh(tempMesh);
    VertexData.ComputeNormals(
      vertexData.positions,
      vertexData.indices,
      vertexData.normals
    );
    tempMesh.dispose();
    vertexData.applyToMesh(theShapeMesh, true);
    theShapeMesh.position = new Vector3(position[0], position[1], position[2]);

    shapeMesh = theShapeMesh;
  }

  return <div>{shape.name}</div>;
}

export default Shape;
