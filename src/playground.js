var createScene = function () {
    BABYLON.SceneLoader.ShowLoadingScreen = false;

    // This creates a basic Babylon Scene object (non-mesh)
    var scene = new BABYLON.Scene(engine);

    // This creates and positions a free camera (non-mesh)
    var camera = new BABYLON.ArcRotateCamera("camera1", 0, 0, 15, BABYLON.Vector3(0,0,0), scene);
    camera.layerMask=1

    // This targets the camera to scene origin
    camera.setTarget(BABYLON.Vector3.Zero());

    // This attaches the camera to the canvas
    camera.attachControl(canvas, true);

    // 
    var light = new BABYLON.HemisphericLight("light", new BABYLON.Vector3(0, 0, 1), scene);

    // Default intensity is 1. Let's dim the light a small amount
    light.intensity = 0.7;

    // Our built-in 'sphere' shape.

////////////////////////////////////////////////////////////////////////////
// KG START
////////////////////////////////////////////////////////////////////////////

// Fix for conversion to rightHanded. Swap axis, and change up position
    scene.useRightHandedSystem = true
    camera.upVector=new BABYLON.Vector3.Forward
    camera.setPosition(new BABYLON.Vector3(0, -15, 15))
    camera.wheelPrecision=40
    camera.pinchPrecision=40

////////////////////////////////////////////////////////////////////////////
// Voxel data model. Voxel is the 3D voxelspace.
////////////////////////////////////////////////////////////////////////////

    class Voxel {
        _dimensions={x:0, y:0, z:0}
        _state=[]
        _weight=0
        _name=""

        get x() { return this._dimensions.x}
        set x(v) { this.setSize(v, this.y, this.z); return v }
        get y() { return this._dimensions.y}
        set y(v) { this.setSize(this.x, v, this.z); return v }
        get z() { return this._dimensions.z}
        set z(v) { this.setSize(this.x, this.y, v); return v }
        get name() { return this._name }
        set name(n) { this._name = n}

        constructor(x=0, y=0, z=0, state) {
            this.setSize(x, y, z)
            if (state) this.stateString = state
        }
        getVoxelState(x, y, z) { return this._state[x][y][z].state}
        setVoxelState(x, y, z, s) { this._state[x][y][z].state=s}
        getVoxelObject(x, y, z) { return this._state[x][y][z]}
        setSize(x, y, z) { 
            this._dimensions={x:x, y:y, z:z}
            for (let x=0;x<=this.x-1;x++) {
                if (this._state[x] == undefined) this._state[x]=[];
                for (let y=0;y<=this.y-1;y++) {
                    if (this._state[x][y] == undefined) this._state[x][y]=[];
                    for (let z=0;z<=this.z-1;z++) {
                        if (this._state[x][y][z] == undefined) this._state[x][y][z]={state:0}
                    }
                }
            }
        }
        get stateString() {
            var ss = ""
            for (let z=0;z<=this.z-1;z++) {
                for (let y=0;y<=this.y-1;y++) {
                    for (let x=0;x<=this.x-1;x++) {
                        switch(this._state[x][y][z].state) {
                            case 0:
                                ss+="_"
                                break;
                            case 1:
                                ss+="#"
                                break;
                            default:
                                ss+="+"
                        }
                    }
                }
            }
            return ss
        }
        set stateString(s) {
            let colorlessStateString = s.replace(/\d+/g,"")
            let tv=0
            for (let x=0;x<=this.x-1;x++) {
                for (let y=0;y<=this.y-1;y++) {
                    for (let z=0;z<=this.z-1;z++) {
                        tv=colorlessStateString[x + y*this.x + z*this.x*this.y]
                        switch(tv) {
                            case "+":
                                this._state[x][y][z].state=2
                                break;
                            case "#":
                                this._state[x][y][z].state=1
                                break;
                            default:
                                this._state[x][y][z].state=0
                        }
                    }
                }
            }
        }
    } // end of class Voxel

    class Puzzle {
        _shapes = []
        get numberOfShapes() { return this._shapes.length}
        get shapes() { return this._shapes }
        set shapes(voxelList) {
            this._shapes.length=0
            for (let v of voxelList) {
                let s=new Voxel(parseInt(v.x), parseInt(v.y), parseInt(v.z), v._text)
                s.name=v.name
                this.addShape(s)
            }
        }
        constructor() {

        }
        cloneShape(s) {
            if (s) return this._shapes.push(new Voxel(s.x, s.y, s.z, s.stateString))
            else return this._shapes.push(new Voxel(2, 2, 2))
        }
        addShape(s=null) {
            if (s==null){
                s = new Voxel(2,2,2,"#_______")
                s.name="anonymous"
            }
            return this._shapes.push(s)
        }
        swapShapes(x,y) {
            let tmp=this._shapes[x]
            this._shapes[x]=this._shapes[y]
            this._shapes[y]=tmp
        }
        removeShape(s) {
            let idx=s
            if (idx <0 || idx >= this._shapes.length) return
            let l = this.shapes.length
            while (idx < l - 1) {
                this.swapShapes(idx, idx+1)
                idx++
            }
            this._shapes.pop()
        }
        getShape(idx) { return this.shapes[idx]}
    }

////////////////////////////////////////////////////////////////////////////
// Visual 3D Voxel editor
////////////////////////////////////////////////////////////////////////////

// From here on we have the visual representation using Babylonjs.
// Box maps to the voxels. Clicking the box changes the state (empty->filled->variable->empty...)
    const BoxMaterials={RW: [   {material: new BABYLON.StandardMaterial("myMaterial", scene), alpha: 0, color: new BABYLON.Color3(1, 0, 0)}, // empty
                                    {material: new BABYLON.StandardMaterial("myMaterial", scene), alpha: 1, color: new BABYLON.Color3(0, 1, 0)}, // filled
                                    {material: new BABYLON.StandardMaterial("myMaterial", scene), alpha: 1, color: new BABYLON.Color3(1, 1, 0)}, // variable
                                ],  
                        RO: [ {material: new BABYLON.StandardMaterial("myMaterial", scene), alpha: 0, color: new BABYLON.Color3(1, 0, 0)},
                                    {material: new BABYLON.StandardMaterial("myMaterial", scene), alpha: 1, color: new BABYLON.Color3(0, 1, 0)},
                                    {material: new BABYLON.StandardMaterial("myMaterial", scene), alpha: 0, color: new BABYLON.Color3(1, 1, 0)},
                        ],
                        HIDDEN: [   {material: new BABYLON.StandardMaterial("myMaterial", scene), alpha: 0, color: new BABYLON.Color3(1, 0, 0)},
                                    {material: new BABYLON.StandardMaterial("myMaterial", scene), alpha: 0.2, color: new BABYLON.Color3(0, 1, 0)},
                                    {material: new BABYLON.StandardMaterial("myMaterial", scene), alpha: 0, color: new BABYLON.Color3(1, 1, 0)},
                ]};
    for (i=0; i<3;i++) {
        for (type in BoxMaterials) {
            BoxMaterials[type][i].material.diffuseColor = BoxMaterials[type][i].color
            BoxMaterials[type][i].material.alpha = BoxMaterials[type][i].alpha
        }
    }
    var isPainting=false;
    class Box {
        //position
        _position={x:0, y:0, z:0}
        _isActive=true // is it pickable/editable?
        _isHighlighted=false // is it highlighted?
        _isVisible=true // is it Visible?
        _mesh=null
        _stateObject=null
        _offset=0
        get materials() { return BoxMaterials }
        get x() { return this._position.x }
        get y() { return this._position.y }
        get z() { return this._position.z }
        get readOnly() { return this._mesh.parent._readOnly }
        constructor(x=0,y=0,z=0,state, parent) {
            this._position = {x:x, y:y, z:z}
            this._stateObject=state
            this._mesh=BABYLON.MeshBuilder.CreateBox("box", {size:1-2*this._offset}, scene)
            if (parent) this._mesh.parent=parent
            this._mesh.position=new BABYLON.Vector3(x, y, z)

            this._mesh.actionManager = new BABYLON.ActionManager(scene);
            this._mesh.actionManager.registerAction(
                new BABYLON.ExecuteCodeAction(
                    BABYLON.ActionManager.OnLeftPickTrigger, (evt) => {
                        if (this.readOnly) return
                        isPainting=true;
                        this.highlight("highlightBox", true)
                        x=evt.source.position.x
                        y=evt.source.position.y
                        z=evt.source.position.z
                        this._stateObject.state+=1;
                        this._stateObject.state%=3;
                        this.render()
                    }
                )
            )
            this._mesh.actionManager.registerAction(
                new BABYLON.ExecuteCodeAction(
                    BABYLON.ActionManager.OnPickUpTrigger, (evt) => {
                        isPainting=false;
                        this.highlight("highlightBox", false)
//                        this.render()
                    }
                )
            )
            this._mesh.actionManager.registerAction(
                new BABYLON.ExecuteCodeAction(
                    BABYLON.ActionManager.OnPointerOverTrigger, (evt) => {
                        if (!this.readOnly && isPainting) {
                            this.highlight("highlightBox", true)
                            x=evt.source.position.x
                            y=evt.source.position.y
                            z=evt.source.position.z
                            this._stateObject.state+=1;
                            this._stateObject.state%=3;
                            this.render()
                        }
                    }
                )
            )
            this._mesh.actionManager.registerAction(
                new BABYLON.ExecuteCodeAction(
                    BABYLON.ActionManager.OnPointerOutTrigger, (evt) => {
                        if (!this.readOnly) {
                            this.highlight("highlightBox", false)
                        }
                    }
                )
            )
        }
        render() {
            let type=this.readOnly?"RO":"RW"
            let m=this.materials[type][this._stateObject.state].material
            this._mesh.isVisible=this._isVisible
            this._mesh.material=m
            this._mesh.isPickable=(this._isActive && !this.readOnly) || (this.readOnly && m.alpha == 1)
            if (this.readOnly) {
                this._mesh.disableEdgesRendering()
            } else {
                if (this._isActive) {
                    this._mesh.enableEdgesRendering()
                } else { 
                    this._mesh.material=this.materials["HIDDEN"][this._stateObject.state].material
                    this._mesh.disableEdgesRendering()
                }
            }
        }
        highlight(layerName, state) {
            let hl=null
            try { hl=scene.getHighlightLayerByName(layerName)} catch {}
            if ( hl == undefined ) {hl = new BABYLON.HighlightLayer(layerName, scene);hl.layerMask=1}
            if (state)
                hl.addMesh(this._mesh, BABYLON.Color3.White(), true)
            else
                hl.removeMesh(this._mesh)
        }
        setState(val) {
            this._stateObject.state=val;
            this._stateObject.state%=3;
        }
        dispose() {
            this._mesh.dispose()
        }
    }

    class Grid {
        // dimension of grid
        _dimensions={x:0, y:0, z:0}
        _voxel=null
        _boxes=[] // will become a 3 dimensional array [x][y][z]
        _layerIsActive={x:[false], y:[false], z:[false]}
        _parent=null
        set position(p) { this._parent.position = p }
        get position() { return this._parent.position }
        get dimensions() { return this._dimensions }
        get x() { return this._dimensions.x }
        get y() { return this._dimensions.y }
        get z() { return this._dimensions.z }
        get readOnly() { return this._parent._readOnly }
        set readOnly(val) {
            this._parent._readOnly=val
        }
        constructor(voxel=null, parent=null) {
            if (parent == null) this._parent = new BABYLON.TransformNode("root")
            else this._parent=parent
            this.readOnly=false
            if (voxel == null) this._voxel=new Voxel()
            else this.attach(voxel)
        }
        setSize(x, y, z) {
            this._dimensions = {x:x, y:y, z:z}
            this._voxel.setSize(this.x,this.y,this.z)
            for (let x=0;x<=this.x-1;x++) {
                if ( this._layerIsActive.x[x]==undefined ) this._layerIsActive.x[x]=false
                if (this._boxes[x] == undefined) { 
                    this._boxes[x]=[];                 
                }
                for (let y=0;y<=this.y-1;y++) {
                    if ( this._layerIsActive.y[y]==undefined ) this._layerIsActive.y[y]=false
                    if (this._boxes[x][y] == undefined) {
                        this._boxes[x][y]=[];
                    }
                    for (let z=0;z<=this.z-1;z++) {
                        if ( this._layerIsActive.z[z]==undefined ) this._layerIsActive.z[z]=false
                        if (this._boxes[x][y][z] == undefined) { 
                            this._boxes[x][y][z]=new Box(x,y,z,this._voxel.getVoxelObject(x,y,z), this._parent)
                        }
                    }
                }
            }
        }
        render() {
            let cx=this._layerIsActive.x.length;
            let cy=this._layerIsActive.y.length;
            let cz=this._layerIsActive.z.length
            for (let x=0;x<cx;x++) {
                for (let y=0;y<cy;y++) {
                    for (let z=0;z<cz;z++) {
                        if ( this._boxes[x] !== undefined && this._boxes[x][y] !== undefined && this._boxes[x][y][z] !== undefined) {
                            this._boxes[x][y][z]._isVisible=( x < this.x && y < this.y && z < this.z )
                            this._boxes[x][y][z]._isActive= ( this._layerIsActive.x[x] || this._layerIsActive.y[y] || this._layerIsActive.z[z])
                            this._boxes[x][y][z].render(this.readOnly)
                        }
                    }
                }
            }
        }
        highlight(layerName, layerNumber=0, state=false) {
            // if layerName is not either "x" or "y" or "z" the highlight will apply to the full grid
            let xmin=(layerName=="x")?layerNumber:0;
            let ymin=(layerName=="y")?layerNumber:0;
            let zmin=(layerName=="z")?layerNumber:0
            let xmax=(layerName=="x")?layerNumber:this.x-1
            let ymax=(layerName=="y")?layerNumber:this.y-1
            let zmax=(layerName=="z")?layerNumber:this.z-1
            for (let x=xmin;x<=xmax;x++) {
                for (let y=ymin;y<=ymax;y++) {
                    for (let z=zmin;z<=zmax;z++) {
                        this._boxes[x][y][z].highlight("highlightGrid", state)
                    } 
                }
            }
        }
        setLayerState(layerName, layerNumber=0, state=0) {
            // if layerName is not either "x" or "y" or "z" the state will apply to the full grid
            let xmin=(layerName=="x")?layerNumber:0;
            let ymin=(layerName=="y")?layerNumber:0;
            let zmin=(layerName=="z")?layerNumber:0
            let xmax=(layerName=="x")?layerNumber:this.x-1
            let ymax=(layerName=="y")?layerNumber:this.y-1
            let zmax=(layerName=="z")?layerNumber:this.z-1
            for (let x=xmin;x<=xmax;x++) {
                for (let y=ymin;y<=ymax;y++) {
                    for (let z=zmin;z<=zmax;z++) {
                        this._boxes[x][y][z].setState(state)
                    } 
                }
            }
        }
        dispose() {
            let cx=this._layerIsActive.x.length;
            let cy=this._layerIsActive.y.length;
            let cz=this._layerIsActive.z.length
            for (let x=0;x<cx;x++) {
                for (let y=0;y<cy;y++) {
                    for (let z=0;z<cz;z++) {
                        if ( this._boxes[x] !== undefined && this._boxes[x][y] !== undefined && this._boxes[x][y][z] !== undefined) {
                            this._boxes[x][y][z].dispose()
                        }
                    }
                }
            }
            this._dimensions={x:0, y:0, z:0}
            this._voxel=new Voxel()
            this._boxes=[] // will become a 3 dimensional array [x][y][z]
            this._layerIsActive={x:[false], y:[false], z:[false]}
        }
        attach(voxel) {
            this.dispose()
            this._voxel=voxel
            this.setSize(voxel.x, voxel.y, voxel.z)
            this.render()
        }
    } // end of class Voxel

    var advancedTexture = BABYLON.GUI.AdvancedDynamicTexture.CreateFullscreenUI("BurrUI");
    advancedTexture.layer.layerMask=1
    const ControlMaterials={ x: {material: new BABYLON.StandardMaterial("xMaterial", scene), alpha: 1, color: new BABYLON.Color3(1, 0, 0), rotation: new BABYLON.Vector3(0,0,-Math.PI/2)},
                    y: {material: new BABYLON.StandardMaterial("yMaterial", scene), alpha: 1, color: new BABYLON.Color3(0, 1, 0), rotation: new BABYLON.Vector3(0,0,0)},
                    z: {material: new BABYLON.StandardMaterial("zMaterial", scene), alpha: 1, color: new BABYLON.Color3(0, 0, 1), rotation: new BABYLON.Vector3(Math.PI/2,0,0)},
                    origin: {material: new BABYLON.StandardMaterial("originMaterial", scene), alpha: 1, color: new BABYLON.Color3(1, 1, 1)},
                    layerSelected: {material: new BABYLON.StandardMaterial("originMaterial", scene), alpha: 1, color: new BABYLON.Color3(1, 1, 0)},
                };
    for (i in ControlMaterials) {
            ControlMaterials[i].material.diffuseColor = ControlMaterials[i].color
    }
    class GridControls {
        _grid=null
        _dimensions={x:0, y:0, z:0}
        _controlSize=0.3
        _offsetControls=0.3/2 + 0.5
        _controls={x:[], y:[], z:[], origin:null, scalers:{x:null, y:null, z:null}, axis:{x:null, y:null, z:null}, scalerLabels:{x:null, y:null, z:null}}
        _parent=null
        get dimensions() { return this._dimensions }
        get materials() { return ControlMaterials }
        get x() { return this._dimensions.x }
        set x(val) { this._dimensions.x=val }
        get y() { return this._dimensions.y }
        get z() { return this._dimensions.z }
        get readOnly() { return this._grid.readOnly }
        set readOnly(val) {
            this._grid.readOnly=val
            return this.readOnly
        }
        reSize(axisName, newSize) { 
            this._dimensions[axisName]=newSize
            this.setSize(this.x, this.y, this.z)
        } 
        setSize(x, y, z) {
            this._dimensions={x:x, y:y, z:z}
            this._grid.setSize(x,y,z)
        }
        syncWithGrid() {
            this._dimensions=this._grid.dimensions
            this.render()
        }
        constructor(grid){
            this._grid=grid;
            this._parent=grid._parent
            // The layer selectors (created in setSize)
            this.setSize(grid.x, grid.y, grid.z)
            scene.activeCamera.setTarget(new BABYLON.Vector3((this.x-1)/2, (this.y-1)/2, (this.z-1)/2));
            // The Origin
            this._controls.origin=BABYLON.MeshBuilder.CreateSphere("origin", {diameter:this._controlSize} , scene)
            this._controls.origin.parent=this._parent
            let m=this.materials.origin.material
            this._controls.origin.material=m
            this._controls.origin.position=new BABYLON.Vector3(-this._offsetControls, -this._offsetControls, -this._offsetControls)
            this._controls.origin.actionManager = new BABYLON.ActionManager(scene);
            this._controls.origin.actionManager.registerAction(
                new BABYLON.ExecuteCodeAction(
                    BABYLON.ActionManager.OnPickTrigger, (evt) => {
                        scene.activeCamera.setTarget(new BABYLON.Vector3(this._parent.position.x+(this.x-1)/2, this._parent.position.y+(this.y-1)/2, this._parent.position.z+(this.z-1)/2));
                    }
                )
            )
            this._controls.origin.actionManager.registerAction(
                new BABYLON.ExecuteCodeAction(
                    BABYLON.ActionManager.OnLongPressTrigger, (evt) => {
                        this.readOnly=!this.readOnly
                        this.render()
                    }
                )
            )
            // The Axis
            for (let axis of ["x","y","z"]) {
                this._controls.axis[axis]=BABYLON.MeshBuilder.CreateCylinder("axis_"+axis, {diameter:this._controlSize/5, height:1}, scene)
                this._controls.axis[axis].parent=this._parent
                this._controls.axis[axis].material = this.materials[axis].material
                this._controls.axis[axis].setPivotMatrix(BABYLON.Matrix.Translation(0,0.5,0), false);
                this._controls.axis[axis].position=new BABYLON.Vector3(-this._offsetControls, -this._offsetControls, -this._offsetControls)
                this._controls.axis[axis].rotation = this.materials[axis].rotation
                this._controls.axis[axis].scaling = new BABYLON.Vector3(1,this.dimensions[axis]+this._offsetControls,1)
            }
            // The Scalers
            for (let axis of ["x","y","z"]) {
                this._controls.scalers[axis]=BABYLON.MeshBuilder.CreateCylinder("scaler_"+axis, {diameterBottom:this._controlSize, diameterTop:0, height:0.5}, scene)
                this._controls.scalers[axis].parent=this._parent
                this._controls.scalers[axis].material = this.materials[axis].material
                this._controls.scalers[axis].rotation = this.materials[axis].rotation
                this._controls.scalers[axis].position=new BABYLON.Vector3(-this._offsetControls, -this._offsetControls, -this._offsetControls)
                this._controls.scalers[axis].locallyTranslate(new BABYLON.Vector3(0, this.dimensions[axis]+this._offsetControls, 0))
                var fullDistance=0; var targetP=0; var startP=0; 
                var drag = new BABYLON.PointerDragBehavior({dragAxis: BABYLON.Vector3.Up()});
                drag.validateDrag = function(pos){return false}
                drag.onDragStartObservable.add((e)=>{
                    startP=Math.round(this._controls.scalers[axis].position[axis]);
                    targetP=startP;fullDistance=0;
                    this._controls.scalerLabels[axis].isVisible=true;
                    this.setLabel(axis,targetP.toString())
                })
                drag.onDragObservable.add((e) => {
                    fullDistance+=e.dragDistance;
                    let ot=targetP;
                    targetP=(Math.max(startP+Math.sign(fullDistance)*Math.floor(Math.abs(fullDistance)),1));
                    if (ot != targetP) {
                        this.setLabel(axis,targetP.toString());
                        this.reSize(axis,targetP);
                        this.render();
                    }
                })
                drag.onDragEndObservable.add((e)=>{
                    this.render(); 
                    this._controls.scalerLabels[axis].isVisible=false;
                    this.setLabel(axis,axis)
                })
                this._controls.scalers[axis].addBehavior(drag)
                this._controls.scalers[axis].actionManager = new BABYLON.ActionManager(scene);
            }
            // The Scaler Labels
            for (let axis of ["x","y","z"]) {
                let lr = new BABYLON.GUI.Rectangle();
                let l = new BABYLON.GUI.TextBlock("label",axis);
                advancedTexture.addControl(lr);
                lr.linkWithMesh(this._controls.scalers[axis]);
                lr.addControl(l);
                lr.width = 0.08;
                lr.height = "40px";
                lr.cornerRadius = 20;
                lr.color = "Orange";
                lr.thickness = 4;
                lr.background = "green";
                lr.linkOffsetX = -50;
                lr.linkOffsetY = 50;
                lr.isVisible=false
                this._controls.scalerLabels[axis] = lr
            }
            this.render()
        }
        setLabel(axis, text) {
            let l=this._controls.scalerLabels[axis].getChildByName("label")
            l.text=axis+ " : " + text
        }
        render() {
            // hide the layer selectors
            for (let axis of ["x","y","z"]) {
            }
            // scale the axis
            for (let axis of ["x","y","z"]) {
                for (let idx=this.dimensions[axis]; idx < this._controls[axis].length;idx++) {
                    this._controls[axis][idx].isVisible=false
                }
                for (let idx=0; idx < this.dimensions[axis];idx++) {
                    // create the layer selectors (only) if needed
                    if (this._controls[axis][idx] == undefined) { 
                        this._controls[axis][idx]=BABYLON.MeshBuilder.CreateBox("box", {width:this._controlSize, height:0.1, depth:this._controlSize}, scene)
                        this._controls[axis][idx].parent=this._parent
                        this._controls[axis][idx].material = this.materials[axis].material
                        this._controls[axis][idx].rotation = this.materials[axis].rotation
                        this._controls[axis][idx].position=new BABYLON.Vector3(-this._offsetControls, -this._offsetControls, -this._offsetControls)
                        this._controls[axis][idx].locallyTranslate(new BABYLON.Vector3(0, idx+this._offsetControls, 0))
                        this._controls[axis][idx].actionManager = new BABYLON.ActionManager(scene);
                        this._controls[axis][idx].actionManager.registerAction(
                            new BABYLON.ExecuteCodeAction(
                                BABYLON.ActionManager.OnPickTrigger, (evt) => {
                                    this._grid._layerIsActive[axis][idx]=!this._grid._layerIsActive[axis][idx]
                                    this.render()
                                }
                            )
                        )
                        this._controls[axis][idx].actionManager.registerAction(
                            new BABYLON.ExecuteCodeAction(
                                BABYLON.ActionManager.OnLongPressTrigger, (evt) => {
                                    if (!this.readOnly) this._grid.highlight(axis,idx,true)
                                }
                            )
                        )
                        this._controls[axis][idx].actionManager.registerAction(
                            new BABYLON.ExecuteCodeAction(
                                BABYLON.ActionManager.OnPointerOutTrigger, (evt) => {
                                    this._grid.highlight(axis,idx,false)
                                }
                            )
                        )
                    }
                    this._controls[axis][idx].isVisible=!this.readOnly
                    if (this._grid._layerIsActive[axis][idx]) {
                        this._controls[axis][idx].material = this.materials.layerSelected.material
                    } else {
                        this._controls[axis][idx].material = this.materials[axis].material
                    }
                }
                // resize the axis
                this._controls.axis[axis].scaling = new BABYLON.Vector3(1,this.dimensions[axis]+this._offsetControls,1)
                // move the scalers to the end of the axis
                let p=this._controls.scalers[axis].getPositionExpressedInLocalSpace()
                this._controls.scalers[axis].setPositionWithLocalVector(new BABYLON.Vector3(p.x, this.dimensions[axis], p.z))
                this._controls.scalers[axis].isVisible=!this.readOnly
            }
            // render the child grid
            this._grid.render()
        }
    } // end of class GridControls

//  Following logic prevents the camera from rotating when click starts on a mesh
//  Additionally it temporarily sets the editor to read-only when the camera is being rotated
    scene.onPointerObservable.add((pointerInfo) => {
        switch (pointerInfo.type) {
            case BABYLON.PointerEventTypes.POINTERDOWN:
//                oldRO=controls.readOnly
                if (pointerInfo.pickInfo.pickedMesh) { 
                    camera.detachControl(canvas) 
                }
                break;
            case BABYLON.PointerEventTypes.POINTERUP:
                isPainting=false
                camera.attachControl(canvas, true)
                break;
        }
    });

////////////////////////////////////////////////////////////////////////////
// The Menu
////////////////////////////////////////////////////////////////////////////

    class OptionListControl {
        _optionList=[] // array of { name, ... }. The name property is used as display value in the list
        _selectedIndex=-1
        _sv=null // scrollViewer
        _panel=null // stackPanel with option buttons
        _grid=null // grid with scrollviewer and decorators
        _buttonLeft=null
        _buttonRight=null
        _buttonRemove=null
        _buttonAdd=null
        _buttonClone=null
        // The function that will be called on selection of an item.
        // idx: index of the selected item
        // use setCallback( (idx) => {...} ) to override
        _selectCallback = function(idx) { return null} 
        _leftCallback = function(e) { return null}
        _rightCallback = function(idx) { return null}
        _addCallback = function(idx) { return null}
        _removeCallback = function(idx) { return null}
        _cloneCallback = function(idx) { return null}

        get control() { return this._grid}
        get options() {return this._optionList}
        set options(s) { 
            this._optionList=s
            this.render()
        }
        get items() { return this._panel.getDescendants(true) }
        get checkedOption() {
            if (this.checkedIndex >=0) { return this.options[this.checkedIndex]}
            else return null
        }
        get checkedIndex() {
            return this._selectedIndex
        }
        set checkedIndex(i) {
            this._selectedIndex=i
            let list=this._panel.getDescendants(true)
            for (let idx in list) { // iterate over the items in the list
                list[idx].getChildByName("radio").isChecked = (idx == i)
            }
            this._selectCallback(i)
        }
        set selectCallback(f) { this._selectCallback=f }
        set leftCallback(f) { this._leftCallback=f }
        set rightCallback(f) { this._rightCallback=f }
        set addCallback(f) { this._addCallback=f }
        set removeCallback(f) { this._removeCallback=f }
        set cloneCallback(f) { this._cloneCallback=f }
        _addButton(idx) {
            var button = new BABYLON.GUI.RadioButton("radio");
            button.width = "20px";
            button.height = "20px";
            button.color = "white";

            button.onIsCheckedChangedObservable.add((state) => {
                if (state) {
                    this._selectedIndex=idx
                    this._selectCallback(idx)
                }
            }); 

            var item = BABYLON.GUI.Control.AddHeader(button, this.options[idx].name, "190px", { isHorizontal: true, controlFirst: true });
            item.height = "30px";
            item.color = "white"
            this._panel.addControl(item);    
        }
        constructor(control=null) {
            // Grid containing
            //    1 row with
            //       1 left button
            //       1 right button
            //    1 row with
            //       1 ScrollViewer containing 
            //          1 Stackpanel
            //    1 row with
            //       1 add button
            //       1 clone button
            //       1 remove button
            let grid = new BABYLON.GUI.Grid("OptionList grid");
            grid.addRowDefinition(50, true)
            grid.addRowDefinition(1)
            grid.addRowDefinition(50, true)
            this._grid=grid
            // ScrollViewer
            let sv = new BABYLON.GUI.ScrollViewer();
            sv.thickness = 2;
            sv.alpha=1
            sv.color = "orange";
            sv.width = 1;
            sv.setPaddingInPixels(10)
            sv.background = "black";
            this._sv=sv
            grid.addControl(sv, 1, 0)
            // Panel within ScrollViewer
            let panel = new BABYLON.GUI.StackPanel("thePanel");    
            this._panel=panel
            var mouseState=null
            panel.onPointerDownObservable.add((evt) => {mouseState={pos:evt.y, pct:sv.verticalBar.value}})
            panel.onPointerUpObservable.add((evt) => {mouseState=null})
            panel.onPointerMoveObservable.add((evt) => {
                if (mouseState!=null) {let delta=(evt.y-mouseState.pos)/(panel.heightInPixels-sv.heightInPixels);sv.verticalBar.value=mouseState.pct-delta}
            })
            sv.isPointerBlocker=true
            sv.addControl(panel)
            // left/right decorators
            let buttonLeft = BABYLON.GUI.Button.CreateImageOnlyButton(
                "button_left",
                "https://kgeusens.gitlab.io/burr-react/arrow-left-circle.svg#"
            );
            buttonLeft.image.stretch = BABYLON.GUI.Image.STRETCH_UNIFORM;
            buttonLeft.width="50px"
            buttonLeft.color="transparent"
            buttonLeft.horizontalAlignment=0
            buttonLeft.paddingLeftInPixels=10
            buttonLeft.paddingTopInPixels=10
            buttonLeft.onPointerClickObservable.add((e) => this._leftCallback())
            grid.addControl(buttonLeft,0,0)
            this._buttonLeft=buttonLeft
            let buttonRight = BABYLON.GUI.Button.CreateImageOnlyButton(
                "button_right",
                "https://kgeusens.gitlab.io/burr-react/arrow-right-circle.svg#"
            );
            buttonRight.image.stretch = BABYLON.GUI.Image.STRETCH_UNIFORM;
            buttonRight.width="50px"
            buttonRight.color="transparent"
            buttonRight.horizontalAlignment=1
            buttonRight.paddingRightInPixels=10
            buttonRight.paddingTopInPixels=10
            buttonRight.onPointerClickObservable.add((e) => this._rightCallback())
            grid.addControl(buttonRight,0,0)
            this._buttonRight=buttonRight
            // add/remove/clone decorators
            let buttonPlus = BABYLON.GUI.Button.CreateImageOnlyButton(
                "button_right",
                "https://kgeusens.gitlab.io/burr-react/node-plus.svg#"
            );
            buttonPlus.image.stretch = BABYLON.GUI.Image.STRETCH_UNIFORM;
            buttonPlus.width="50px"
            buttonPlus.color="transparent"
            buttonPlus.horizontalAlignment=0
            buttonPlus.paddingLeftInPixels=10
            buttonPlus.paddingBottomInPixels=10
            buttonPlus.onPointerClickObservable.add((e) => this._addCallback())
            grid.addControl(buttonPlus,2,0)
            this._buttonPlus=buttonPlus
            let buttonMinus = BABYLON.GUI.Button.CreateImageOnlyButton(
                "button_right",
                "https://kgeusens.gitlab.io/burr-react/node-minus.svg#"
            );
            buttonMinus.image.stretch = BABYLON.GUI.Image.STRETCH_UNIFORM;
            buttonMinus.width="50px"
            buttonMinus.color="transparent"
            buttonMinus.horizontalAlignment=1
            buttonMinus.paddingRightInPixels=10
            buttonMinus.paddingBottomInPixels=10
            buttonMinus.onPointerClickObservable.add((e) => this._removeCallback())
            grid.addControl(buttonMinus,2,0)
            this._buttonMinus=buttonMinus

            if (control !== null) control.addControl(grid);
        }
        render() {
            // remove the excess entries
            for (let idx=this.options.length;idx<this.items.length;idx++) {
                this._panel.removeControl(this.items[idx])
            }
            // create the missing entries
            for (let idx=this.items.length;idx<this.options.length;idx++) {
                this._addButton(idx)
            }
            // assign the correct labels
            for (let idx in this.items) { // iterate over the items in the list
                this.items[idx].getChildByName("header").text=this.options[idx].name
            }
        }
    }

    // hack to allow animation of menu
    BABYLON.GUI.Control.prototype.getScene = function () { return scene };
    class MenuPanel {
        _item=null
        _panel=null
        _showing=false
        _ease=null
        _width=320
        get control() { return this._item }
        get menuPanel() { return this._panel}
        get isShowing() { return this._showing }
        get width() { return this._width }
        get ease() { return this._ease }
        get panelWidth() { return (this._width - 40) }
        get width() { return this._width }
        set isShowing(val) {
            if (val != this._showing) {
                if (val) BABYLON.Animation.CreateAndStartAnimation("VisAnim", this.control, "left", 10, 10, -this.panelWidth, 0, 0, this._ease)
                else     BABYLON.Animation.CreateAndStartAnimation("VisAnim", this.control, "left", 10, 10, 0, -this.panelWidth, 0, this._ease)
                this._showing = val
            }
        }
        toggle() { this.isShowing = !this.isShowing }
        addControl(control, row=1) { 
            this._panel.addControl(control,row,0)
        }
        constructor(control=null) {
            var container = new BABYLON.GUI.Rectangle("menu");
            var mleft = new BABYLON.GUI.Grid("menu panel");
            var bt = BABYLON.GUI.Button.CreateSimpleButton("menu button", "menu");
            this._ease = new BABYLON.CubicEase();
            this._ease.setEasingMode(BABYLON.CubicEase.EASINGMODE_EASEINOUT)
            this._item=container
            this._panel=mleft

            bt.widthInPixels = 80
            bt.heightInPixels = 30
            bt.color = "orange";
            bt.thickness = 2;
            bt.background = "black";
            bt.verticalAlignment=0
            bt.left=this.panelWidth/2
            bt.top=40
            bt.rotation=Math.PI/2

            mleft.widthInPixels = this.panelWidth
            mleft.addRowDefinition(50, true)
            mleft.addRowDefinition(1)
            mleft.addRowDefinition(50, true)
            mleft.addRowDefinition(50, true)
            mleft.horizontalAlignment=0
            mleft.color = "Orange";
            mleft.thickness = 2;
            mleft.background = "black";
            mleft.alpha=0.7

            container.widthInPixels = this.width
            container.height = 1
            container.thickness = 0
            container.horizontalAlignment=0
            container.left=-this.panelWidth
            container.color = "Green"
            container.background = "transparent"
            container.addControl(mleft)
            container.addControl(bt)
            container.isPointerBlocker=true

            bt.onPointerClickObservable.add((evt) => {this.toggle()})
 
            if (control !== null) { control.addControl(container) }
        }
    }

////////////////////////////////////////////////////////////////////////////
// Test code
////////////////////////////////////////////////////////////////////////////

    // Create puzzle
    var puzzle=new Puzzle
    puzzle.addShape(new Voxel(4,3,2,"#######_##__+++_++__#___"))
    puzzle.shapes[0].name="first shape"

    // Create the grid editor for the shape
    var grid=new Grid // the menu will assign the selected shape to the grid
//    grid.position=new BABYLON.Vector3(0,0,0)
    grid.readOnly=true
    var controls=new GridControls(grid)

    // Create the Menu
    var GuiTexture = BABYLON.GUI.AdvancedDynamicTexture.CreateFullscreenUI("UI");
    var theShapeMenu = new MenuPanel

    // the buttons to manipulate the shapes
    // Great resource for icons is "https://icons.getbootstrap.com/"

    //  inputs
    var inputName = new BABYLON.GUI.InputText("label")
    inputName.width="200px"
    inputName.text="test shape entry"
    inputName.background="black"
    inputName.color="orange"
    inputName.horizontalAlignment=0
    var labeledName = BABYLON.GUI.Control.AddHeader(inputName, "label", "50px", { isHorizontal: true, controlFirst: false });
    labeledName.height = "30px";
    labeledName.color = "orange"

    // the shape selector list
    var theShapeList=new OptionListControl 
    theShapeList.options=puzzle.shapes
    scb=function(idx) {
        if (idx >= 0) {
            grid.attach(this.options[idx]); controls.syncWithGrid(); // update the shape editor
//            buttonMinus.isVisible=(idx>=0) // show/hide left arrow
//            buttonLeft.isVisible=(idx>0) // show/hide left arrow
//            buttonRight.isVisible=(idx < theShapeList.options.length-1) || (idx < 0)
//            buttonMinus.isVisible=(theShapeList.options.length>0)
//            inputName.theShapeList=this
//            inputName.option=this.options[idx]
            inputName.text=this.options[idx].name
        }
        else {
            inputName.text=""
        }
    }
    // set the menu callback functions
    inputName.onTextChangedObservable.add((t) => {if (theShapeList.checkedIndex<0) return;theShapeList.checkedOption.name=t.text;theShapeList.render()})
    theShapeList.selectCallback=scb.bind(theShapeList)
    lcb=function(evt) {let idx=this.checkedIndex;if (idx<=0) return;puzzle.swapShapes(idx, idx-1);this.render();this.checkedIndex=idx-1}
    theShapeList.leftCallback=lcb.bind(theShapeList)
    rcb=function(evt) {let idx=this.checkedIndex;if (idx<0 || idx == this.options.length-1) return;puzzle.swapShapes(idx, idx+1);this.render();this.checkedIndex=idx+1}
    theShapeList.rightCallback=rcb.bind(theShapeList)
    acb=function(evt) {puzzle.addShape();this.render();this.checkedIndex=this.options.length-1}
    theShapeList.addCallback=acb.bind(theShapeList)
    dcb=function(evt) {let idx=this.checkedIndex;puzzle.removeShape(idx);this.checkedIndex=-1;this.render();grid.dispose();controls.syncWithGrid()}
    theShapeList.removeCallback=dcb.bind(theShapeList)
    theShapeList.checkedIndex=0

    // add the menu components in place
    GuiTexture.addControl(theShapeMenu.control)
    theShapeMenu.addControl(theShapeList.control,1)
    theShapeMenu.addControl(labeledName,3)

    // Get an example puzzle from external site.
    // This is async, and especially the first time, can take quite some time (free azure service)
    async function fetchPuzzle() {
        engine.loadingScreen.displayLoadingUI();
        data = await fetch("https://kg-nodejs-1.azurewebsites.net/xmpuzzles/5fa7ff8474cca00393b02426",{headers: {'Content-Type': 'application/json'}})
        engine.loadingScreen.hideLoadingUI();
        return data.json()
    }
    fetchPuzzle().then((res) => { puzzle.shapes=res[0].shapes.voxel;theShapeList.checkedIndex=0 })

    return scene
};