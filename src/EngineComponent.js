import { Engine } from "@babylonjs/core";
import React, { useEffect, useRef, useState } from "react";

const EngineContext = React.createContext({ engine: null, setEngine: null });
const { Provider } = EngineContext;

//////////
// Engine
// the renderloop is based on "VIEWS"
//////////
const EngineProvider = (props) => {
  const reactCanvas = useRef(null);
  const [engine, setEngine] = useState("empty");
  const {
    children,
    antialias,
    engineOptions,
    adaptToDeviceRatio,
    ...rest
  } = props;

  const addScene = (scene) => {
    //    console.log("addScene", scene.uid);
    //    scene.autoClear = false;
  };

  useEffect(() => {
    if (reactCanvas.current) {
      var canvas = document.createElement("canvas");
      const engine = new Engine(canvas, true);
      // Setting the default size of the engine because "engine-canvas" has size zero
      engine.setSize(300, 150);
      setEngine(engine);

      engine.runRenderLoop(function () {
        if (engine.activeView) {
          if (engine.activeView.camera) {
            engine.activeView.camera.getScene().render();
          }
        }
      });

      return () => {
        engine.dispose();
      };
    }
  }, [reactCanvas]);

  return (
    <Provider value={{ engine, setEngine, addScene }}>
      <div ref={reactCanvas}></div>
      {children}
    </Provider>
  );
};

export default EngineProvider;
export { EngineContext };
