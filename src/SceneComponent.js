import { Engine, Scene } from "@babylonjs/core";
import React, { useEffect, useRef, useState, useContext } from "react";
import { EngineContext } from "./EngineComponent";

const SceneContext = React.createContext(null);
const { Provider } = SceneContext;
var theLocalEngine = null; //

const SceneProvider = (props) => {
  const reactCanvas = useRef(null);
  const [scene, setScene] = useState(null);
  const { engine, addScene } = useContext(EngineContext);
  const {
    children,
    antialias,
    engineOptions,
    adaptToDeviceRatio,
    sceneOptions,
    onRender,
    onSceneReady,
    ...rest
  } = props;
  var camera = null;

  const addCamera = (theCamera) => {
    camera = theCamera;
    // When using multiple canvas on single engine, you need to map the scene/camera controls to the canvas controls
    scene.detachControl();
    engine.inputElement = reactCanvas.current;
    camera.attachControl(reactCanvas.current, true);
    scene.attachControl();

    // if there is a parent <Engine> then register a view for this camera
    if (theLocalEngine == null) {
      engine.registerView(reactCanvas.current, camera);
    }
  };

  useEffect(() => {
    // if Canvas is ready
    if (reactCanvas.current) {
      // if there is no parent <Engine>
      if (!engine) {
        theLocalEngine = new Engine(
          reactCanvas.current,
          antialias,
          engineOptions,
          adaptToDeviceRatio
        );
        const scene = new Scene(theLocalEngine, sceneOptions);
        setScene(scene);
        if (scene.isReady()) {
          props.onSceneReady(scene);
        } else {
          scene.onReadyObservable.addOnce((scene) => props.onSceneReady(scene));
        }
        theLocalEngine.runRenderLoop(() => {
          if (typeof onRender === "function") {
            onRender(scene);
          }
          scene.render();
        });
        const resize = () => {
          scene.getEngine().resize();
        };
        if (window) {
          window.addEventListener("resize", resize);
        }

        return () => {
          scene.getEngine().dispose();
          if (window) {
            window.removeEventListener("resize", resize);
          }
        };
      }
      // if there IS a parent <Engine> and it is initialized, we should work with a view
      else if (engine != "empty") {
        const scene = new Scene(engine, sceneOptions);
        setScene(scene);
        if (scene.isReady()) {
          props.onSceneReady(scene);
        } else {
          scene.onReadyObservable.addOnce((scene) => props.onSceneReady(scene));
        }

        return () => {
          engine.unRegisterView(reactCanvas.current);
          scene.dispose();
        };
      }
    }
  }, [reactCanvas, engine]);

  return (
    <Provider value={{ scene, setScene, addCamera }}>
      <canvas className="scene" ref={reactCanvas} {...rest} />
      {children}
    </Provider>
  );
};

export default SceneProvider;
export { SceneContext };
