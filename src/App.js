import React, { useState } from "react";
import "./App.css";
import BurrFileManager from "./BurrFileManager";
import ShapeScene from "./ShapeScene";

function App() {
  const [puzzle, setPuzzle] = useState({});
  var theFile = null; // temporary placeholder for puzzle

  function onFileReady() {
    setPuzzle(theFile);
    console.log(theFile);
    //    console.log(theFile.shapes);
    //    console.log(theFile.problems);
  }
  function newSource(msg) {
    theFile = new BurrFileManager(msg.target.files[0], () => onFileReady());
  }
  return (
    <div className="App">
      <input onChange={newSource} type="file" />
      {puzzle.shapes && (
        <ShapeScene shapes={puzzle.problems[0].solutions[0].shapes} />
      )}
    </div>
  );
}

export default App;
