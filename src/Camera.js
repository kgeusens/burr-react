import React, { useContext, useEffect, useLayoutEffect } from "react";
import { SceneContext } from "./SceneComponent.js";
import { ArcRotateCamera, Vector3 } from "@babylonjs/core";
// import { Vector3, VertexData, CSG, MeshBuilder } from "@babylonjs/core";

function Camera(props) {
  var { name, alpha, beta, radius, target, ...opts } = props;
  const { scene, addCamera } = useContext(SceneContext);
  var theCamera = null;
  useLayoutEffect(() => {
    if (scene && !theCamera) {
      // create the camera if it is not yet defined
      var origin = new Vector3(0, 0, 0);
      theCamera = new ArcRotateCamera(
        name,
        alpha || 0,
        beta || 0,
        radius || 15,
        target || origin,
        scene
      );
      const canvas = scene.getEngine().getRenderingCanvas();
      // register the camera with the parent scene (from context)
      addCamera(theCamera);
      // listen to mouse events on default canvas of engine
      // theCamera.attachControl(canvas, true);
      // use this trick to update properties by name
      for (let prop of Object.getOwnPropertyNames(opts)) {
        Object.getOwnPropertyDescriptors(ArcRotateCamera.prototype)[
          prop
        ].set.call(theCamera, opts[prop]);
      }
    }
  });
  useEffect(() => {
    return () => {
      if (theCamera) theCamera.dispose();
    };
  }, [scene]);

  return <div></div>;
}

export default Camera;
