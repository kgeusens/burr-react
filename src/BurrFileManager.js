import { rotateVoxelpositions, translateVoxelpositions } from "./burrUtils";
const zlib = require("zlib");
const convert = require("xml-js");

class Voxel {
  constructor(value, props) {
    //  props = { x: x, y: y, z: z,
    //            (optional) name, id, rotation, position };
    this._props = props;
    this._voxelString = value;
    this._props.rotation = props.rotation ? props.rotation : 0;
    this._props.position = props.position ? props.position : [0, 0, 0];
    this._props.name = props.name ? props.name : "";
    this._props.id = props.id;
    this._voxelPositions = [];
    this._holePositions = [];
    for (let dx = 0; dx < this.x; dx++) {
      for (let dy = 0; dy < this.y; dy++) {
        for (let dz = 0; dz < this.z; dz++) {
          if (
            this._voxelString[dz * this.x * this.y + dy * this.x + dx] === "#"
          ) {
            this._voxelPositions.push([dx, dy, dz]);
          } else {
            this._holePositions.push([dx, dy, dz]);
          }
        }
      }
    }
  }
  // Accessors
  get x() {
    return this._props.x;
  }
  get y() {
    return this._props.y;
  }
  get z() {
    return this._props.z;
  }
  get name() {
    return this._props.name;
  }
  get id() {
    return this._props.id;
  }
  get rotation() {
    return this._props.rotation;
  }
  get position() {
    return this._props.position;
  }
  set position(newPos) {
    this._props.position = newPos;
  }
  get value() {
    return this._voxelString;
  }
  get voxelPositions() {
    return rotateVoxelpositions(this._voxelPositions, this.rotation);
    //    return result;
  }
  get translatedVoxelPositions() {
    return translateVoxelpositions(this.voxelPositions, this.position);
  }
  get holePositions() {
    return rotateVoxelpositions(this._holePositions, this.rotation);
  }
  get translatedHolePositions() {
    return translateVoxelpositions(this.holePositions, this.position);
  }
  get dimensions() {
    return rotateVoxelpositions([[this.x, this.y, this.z]], this.rotation)[0];
  }
}

class Shape {
  constructor({ id, count }) {
    this._props = { id: id, count: count };
  }
  get id() {
    return this._props.id;
  }
  get count() {
    return this._props.count;
  }
}

class Assembly {
  constructor(value) {
    this._assemblyString = value;
    this._positions = [];
    this._rotations = [];

    var tmpArray = value.split(" ");
    for (let i = 0; i < tmpArray.length / 4; i++) {
      this._positions.push([
        Number(tmpArray[i * 4]),
        Number(tmpArray[i * 4 + 1]),
        Number(tmpArray[i * 4 + 2]),
      ]);
      this._rotations.push(Number(tmpArray[i * 4 + 3]));
    }
  }
  get id() {
    return this._props.id;
  }
  get count() {
    return this._props.count;
  }
  get positions() {
    return this._positions;
  }
  get rotations() {
    return this._rotations;
  }
}

class Problem {
  constructor(
    { shapes, result, solutionlist, puzzleShapes },
    { name, solutions }
  ) {
    this._props = { name: name, solutions: solutions };
    this._shapes = [];
    this._solutions = [];
    this._puzzleShapes = []; // contains the real shape objects to solve the problem - copied from puzzleShapes
    this._result = puzzleShapes[result.id]; // the real shape object of the solution

    this._shapes = shapes.map((s) => new Shape(s._attributes));
    this._shapes.forEach((s) => {
      for (let i = 0; i < s.count; i++) {
        this._puzzleShapes.push(puzzleShapes[s.id]);
      }
    });
    this._solutions = solutionlist.map(
      (l) =>
        new Solution(
          {
            assembly: l.assembly,
            separation: l.separation,
            shapes: this.shapes,
          },
          l._attributes
        )
    );
  }
  // Accessors
  get name() {
    return this._props.name;
  }
  get solutionCount() {
    return this._props.solutions;
  }
  get solutions() {
    return this._solutions;
  }
  get shapes() {
    return this._puzzleShapes;
  }
  get result() {
    return this._result;
  }
}

class Solution {
  constructor({ assembly, separation, shapes }, { solNum }) {
    this._props = { id: solNum };
    this._shapes = [];
    this._separation = new Separation(separation[0]);
    this._assembly = new Assembly(assembly[0]._text[0]);
    // Build the shape list, including position and rotation (calculated from assembly)
    for (let i = 0; i < shapes.length; i++) {
      this._shapes.push(
        new Voxel(shapes[i].value, {
          ...shapes[i]._props,
          id: i,
          rotation: this._assembly.rotations[i],
          position: this._assembly.positions[i],
        })
      );
    }
    // Process the separations
  }
  // Methods
  findSeparation(state) {
    return this._separation.filter(state);
  }
  // Accessors
  get solNum() {
    return this._props.id;
  }
  get shapes() {
    return this._shapes;
  }
}

class Separation {
  constructor({ pieces, state, separation }) {
    this._pieces = pieces[0]._text[0].split(" ").map((n) => Number(n));
    this._separation = [];
    this._states = state.map((s) => new State(s));

    if (separation) this._separation = separation.map((s) => new Separation(s));
  }
  // Methods
  filter(state) {
    this._states.filter((s) => (s.state = state));
  }
  // Accessors
  get states() {
    return this._states.map((s) => s.state);
  }
}

class BurrFileManager {
  constructor(burrFile, callback) {
    this._jsonFile = null;
    this._shapes = [];
    this._problems = [];

    var reader = new FileReader();
    // hint: () => {} syntax for passing callbacks makes sure that "this" is defined in the function
    reader.onload = () => {
      var binData = new Uint8Array(reader.result);
      var charData = zlib.gunzipSync(Buffer.from(binData));
      this._jsonFile = JSON.parse(
        convert.xml2json(charData, {
          nativeType: true,
          nativeTypeAttributes: true,
          alwaysArray: true,
          compact: true,
        })
      );
      // parse the puzzle shapes
      this._shapes = this.source.puzzle[0].shapes[0].voxel.map(
        (v) => new Voxel(v._text[0], v._attributes)
      );
      // parse the problems
      this._problems = this.source.puzzle[0].problems[0].problem.map(
        (p) =>
          new Problem(
            {
              result: p.result[0]._attributes,
              shapes: p.shapes[0].shape,
              solutionlist: p.solutions[0].solution,
              puzzleShapes: this.shapes,
            },
            p._attributes
          )
      );
      callback && callback();
    };
    //    reader.onload.bind(this);
    reader.readAsArrayBuffer(burrFile);
  }
  // Accessors
  get isReady() {
    return this._jsonFile != null;
  }
  get source() {
    return this._jsonFile;
  }
  get shapes() {
    return this._shapes;
  }
  get problems() {
    return this._problems;
  }
}

/*
class Pieces {
  constructor(value) {
    this._piecesString = value;
    this._piecesArray = value.split(" ");
    this._props.count = this.valueArray.length();
  }
  get count() {
    return this._props.count;
  }
  get value() {
    return this._piecesString;
  }
}
*/
class State {
  constructor({ dx, dy, dz }) {
    this._props = {
      dx: dx[0]._text[0],
      dy: dy[0]._text[0],
      dz: dz[0]._text[0],
    };
    this._state = { normalized: null, absolute: null };
    //cache some stuff
    let x = this._props.dx.split(" ").map((s) => Number(s));
    let y = this._props.dy.split(" ").map((s) => Number(s));
    let z = this._props.dz.split(" ").map((s) => Number(s));
    let absoState = [];
    let normState = [];
    for (let i = 0; i < x.length; i++) {
      absoState.push([x[i], y[i], z[i]]);
      //      absoState.push(y[i]);
      //      absoState.push(z[i]);
      normState.push(x[i] - x[0]);
      normState.push(y[i] - y[0]);
      normState.push(z[i] - z[0]);
    }
    this._state = {
      normalized: normState,
      absolute: absoState,
    };
  }
  // Accessors
  get state() {
    return this._state.normalized.join(" ");
  }
}

export default BurrFileManager;
