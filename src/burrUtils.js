const math = require("mathjs");

const transformationMatrix = [
  [
    [1, 0, 0],
    [0, 1, 0],
    [0, 0, 1],
  ],
  [
    [1, 0, 0],
    [0, 0, -1],
    [0, 1, 0],
  ],
  [
    [1, 0, 0],
    [0, -1, 0],
    [0, 0, -1],
  ],
  [
    [1, 0, 0],
    [0, 0, 1],
    [0, -1, 0],
  ],
  [
    [0, 0, -1],
    [0, 1, 0],
    [1, 0, 0],
  ],
  [
    [0, -1, 0],
    [0, 0, -1],
    [1, 0, 0],
  ],
  [
    [0, 0, 1],
    [0, -1, 0],
    [1, 0, 0],
  ],
  [
    [0, 1, 0],
    [0, 0, 1],
    [1, 0, 0],
  ],
  [
    [-1, 0, 0],
    [0, 1, 0],
    [0, 0, -1],
  ],
  [
    [-1, 0, 0],
    [0, 0, -1],
    [0, -1, 0],
  ],
  [
    [-1, 0, 0],
    [0, -1, 0],
    [0, 0, 1],
  ],
  [
    [-1, 0, 0],
    [0, 0, 1],
    [0, 1, 0],
  ],
  [
    [0, 0, 1],
    [0, 1, 0],
    [-1, 0, 0],
  ],
  [
    [0, 1, 0],
    [0, 0, -1],
    [-1, 0, 0],
  ],
  [
    [0, 0, -1],
    [0, -1, 0],
    [-1, 0, 0],
  ],
  [
    [0, -1, 0],
    [0, 0, 1],
    [-1, 0, 0],
  ],
  [
    [0, -1, 0],
    [1, 0, 0],
    [0, 0, 1],
  ],
  [
    [0, 0, 1],
    [1, 0, 0],
    [0, 1, 0],
  ],
  [
    [0, 1, 0],
    [1, 0, 0],
    [0, 0, -1],
  ],
  [
    [0, 0, -1],
    [1, 0, 0],
    [0, -1, 0],
  ],
  [
    [0, 1, 0],
    [-1, 0, 0],
    [0, 0, 1],
  ],
  [
    [0, 0, -1],
    [-1, 0, 0],
    [0, 1, 0],
  ],
  [
    [0, -1, 0],
    [-1, 0, 0],
    [0, 0, -1],
  ],
  [
    [0, 0, 1],
    [-1, 0, 0],
    [0, -1, 0],
  ],
];

const rotationMatrix = [
  [0, 0, 0],
  [1, 0, 0], // OK
  [2, 0, 0], // OK
  [3, 0, 0], // OK
  [0, 3, 0], // OK
  [1, 3, 0], // OK
  [0, 1, 2], // OK
  [3, 3, 0], // OK
  [0, 2, 0], // OK
  [1, 2, 0], // OK
  [0, 0, 2], // OK
  [3, 2, 0], // OK
  [0, 1, 0], // OK
  [1, 1, 0], // OK
  [0, 3, 2], // OK
  [3, 1, 0], // OK
  [0, 0, 1], // OK
  [0, 1, 1], // OK
  [0, 2, 1], // OK
  [0, 3, 1], // OK
  [0, 0, 3], // OK
  [0, 3, 3], // OK
  [0, 2, 3], // OK
  [0, 1, 3], // OK
];

export function rotateVoxelpositions(voxelMatrix, rotationIndex) {
  var T = math.matrix(transformationMatrix[rotationIndex]);
  return math.multiply(voxelMatrix, math.transpose(T)).toArray();
}

export function translateVoxelpositions(voxelMatrix, translation) {
  return voxelMatrix.map((v) => [
    v[0] + translation[0],
    v[1] + translation[1],
    v[2] + translation[2],
  ]);
}

export const KG = "Koen Geusens";

export function rotationVector(idx) {
  var result = [];
  const pie = Math.PI / 2;

  for (let i = 0; i < 3; i++) {
    result.push(rotationMatrix[idx][i] * pie);
  }

  return result;
}

export function rotate(idx) {
  return rotationVector(idx);
}
