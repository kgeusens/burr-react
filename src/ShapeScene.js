import React, { useState, useRef } from "react";
import SceneProvider from "./SceneComponent";
import EngineProvider from "./EngineComponent";
import Shape from "./Shape";
import Camera from "./Camera";
import { Vector3, HemisphericLight } from "@babylonjs/core";

function ShapeScene({ shapes }) {
  const theEngine = useRef(null);
  //////////
  // callback to Initialize Babylon Scene and Engine
  //////////
  const onSceneReady = (scene) => {
    scene.useRightHandedSystem = true; //set to right handed coordinate system
    var light = new HemisphericLight("light", new Vector3(0, 1, 0), scene);
    light.intensity = 0.9;
  };
  //////////
  // JSX
  //////////
  return (
    <div>
      <EngineProvider antialias>
        {shapes.map((voxel) => (
          <SceneProvider
            key={"sc" + voxel.id + voxel.value}
            id="scene-canvas"
            onSceneReady={onSceneReady}
          >
            <Camera
              name="camera1"
              wheelPrecision="20"
              position={new Vector3(20, 15, 20)}
            />
            <Shape
              key={"vx" + voxel.id + voxel.value}
              shape={voxel}
              position={voxel.position}
            />
          </SceneProvider>
        ))}
      </EngineProvider>
    </div>
  );
}

export default ShapeScene;
