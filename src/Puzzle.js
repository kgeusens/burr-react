import React, { useState } from "react";
import Shape from "./Shape";
import { rotationVector } from "./burrUtils.js";

function Puzzle({ shapes }) {
  //////////
  // JSX
  //////////
  return (
    <div>
      {shapes.map((voxel) => (
        <Shape
          key={voxel.id.toString()}
          idx={voxel.id.toString()}
          name={voxel.name}
          shape={voxel.shape}
          position={voxel.position}
          rotation={rotationVector(voxel.rotationIndex)}
        />
      ))}
    </div>
  );
}

export default Puzzle;
